import React from "react";
import { LinearGradient, Svg } from "expo";
import { AppColors, AppSizes } from "@theme";
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: this.props.width ? this.props.width : 10,
      height: this.props.height ? this.props.height : 10,
      color: this.props.color ? this.props.color : AppColors.app_color.secondary
    };
  }
  render() {
    var width = this.props.width ? this.props.width : 10;
    var height = this.props.height ? this.props.height : 10;
    var color = this.props.color ? this.props.color : AppColors.app_color.secondary;
    return (
      <Svg
        width={width}
        height={height}
        viewBox="0 0 512.000000 512.000000"
        preserveAspectRatio="xMidYMid meet"
      >
        <Svg.G
          transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
          fill={color}
          stroke="none"
        >
          <Svg.Path
            d="M4035 5010 c-137 -22 -268 -75 -380 -155 -33 -24 -214 -198 -401
-387 -380 -382 -380 -382 -378 -508 1 -107 61 -203 157 -252 59 -30 159 -35
228 -13 50 16 80 44 400 361 382 380 391 387 506 387 153 0 276 -124 276 -278
0 -122 37 -79 -881 -997 -921 -922 -877 -882 -1002 -882 -64 0 -108 14 -182
62 l-28 17 -202 -202 -202 -202 24 -27 c73 -78 238 -168 375 -205 114 -31 316
-31 430 0 89 24 200 74 272 122 65 43 1753 1729 1808 1805 58 79 109 189 136
289 31 113 31 316 0 430 -97 368 -416 624 -796 640 -55 2 -127 0 -160 -5z"
          />
          <Svg.Path
            d="M2241 3210 c-126 -18 -262 -70 -371 -143 -63 -42 -1552 -1528 -1605
-1603 -58 -79 -109 -189 -136 -289 -31 -113 -31 -316 -1 -430 63 -238 228
-441 447 -550 137 -68 223 -88 385 -88 155 0 244 19 369 78 119 57 182 109
433 362 266 267 281 289 281 403 0 162 -127 290 -288 290 -112 0 -128 -11
-395 -275 -273 -268 -298 -286 -405 -287 -154 -1 -278 122 -278 275 0 123 -29
89 776 895 768 769 772 772 874 788 56 9 134 -11 183 -47 22 -16 43 -29 48
-29 4 0 99 92 210 203 l204 204 -44 37 c-103 89 -227 154 -354 186 -89 23
-245 32 -333 20z"
          />
        </Svg.G>
      </Svg>
    );
  }
}
