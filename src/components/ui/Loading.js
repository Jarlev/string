import React from "react";
import {
  Easing,
  StyleSheet,
  Animated,
  Text,
  View,
  TouchableOpacity,
  Image,
  Modal
} from "react-native";
import { connect } from "react-redux";
import { firebase } from "@constants/";
import Logo from "@components/svgs/Logo";
import AppName from "@components/svgs/AppName";
import { AppColors, AppSizes } from "@theme";
import { Actions } from "react-native-router-flux";
import { LinearGradient, Svg, Font } from "expo";

import * as UserActions from "@redux/user/actions";
const mapStateToProps = state => ({});

// Any actions to map to the component?
const mapDispatchToProps = {
  fontLoader: UserActions.fontLoader
};

class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.state = {
      screenWidth: 0,
      screenHeight: 0,
      showModal: this.props.visible,
      label: this.props.label ? this.props.label : "Loading..."
    };
  }
  componentWillReceiveProps = nextProps => {
    if (nextProps.visible && nextProps.visible == true) {
      this.animate();
    }
    this.setState({
      showModal: nextProps.visible,
      label: nextProps.label ? nextProps.label : "Loading..."
    });
  };
  async componentDidMount(props) {
      if (this.props.visible && this.props.visible == true) {
          this.animate();
      }
  }
  animate = () => {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 3,
      duration: 3000,
      easing: Easing.ease
    }).start(() => {
      if (this.state.showModal) {
        this.animate();
      }
    });
  };
  onLayout = async e => {
    var width = this.state.screenWidth;
    await this.setState({
      screenWidth: e.nativeEvent.layout.width,
      screenHeight: e.nativeEvent.layout.height
    });
  
  };

  render() {
    const Left = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [
        this.state.screenWidth / 2 - this.state.screenWidth * 0.15,
        0,
        this.state.screenWidth - this.state.screenWidth * 0.3,
        this.state.screenWidth / 2 - this.state.screenWidth * 0.15
      ]
    });
    const Top = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],

      outputRange: [
        this.state.screenHeight / 2 - this.state.screenWidth * 0.15,
        this.state.screenHeight / 2 - this.state.screenWidth * 0.15,
        this.state.screenHeight / 2 - this.state.screenWidth * 0.15
      ]
    });
    const spin = this.animatedValue.interpolate({
      inputRange: [0, 3],
      outputRange: ["0deg", "360deg"]
    });
    const radius = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        (this.state.screenWidth * 0.3) / 2,
        20,
        this.state.screenWidth * 0.3
      ]
    });
    const Logo_BG = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        "rgba(113, 194, 255, 1)",
        "rgba(255, 255, 255, 0)",
        "rgba(113, 194, 255, 1)"
      ]
    });
    const Container_BG = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        "rgba(255, 255, 255, 1)",
        "rgba(113, 194, 255, 1)",
        "rgba(255, 255, 255, 1)"
      ]
    });
    const MESSAGE_COLOR = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        "rgba(113, 194, 255, 1)",
        "rgba(255, 255, 255, 1)",
        "rgba(113, 194, 255, 1)"
      ]
    });
    const Logo_opacity = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [1, 1, 1, 0]
    });
    const App_name_opacity = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [0, 0, 0, 1]
    });
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showModal}
        onRequestClose={() => {
          this.setState({ showModal: false });
        }}
      >
        <Animated.View
          onLayout={this.onLayout}
          style={[
            styles.container,
            {
              // backgroundColor: Container_BG
            }
          ]}
        >
          <Animated.View
            style={
              {
                position: "absolute",
              
                //   transform: [{ rotate: spin }],
                justifyContent: "center",
                alignItems: "center",
                borderRadius: radius
              } // left: this.state.screenWidth / 2 - this.state.screenWidth * 0.15,
            }
          >
            <Animated.View
              style={
                {
                  // opacity: App_name_opacity
                }
              }
            >
              <Animated.Text
                style={{
                  color: "white",
                  // fontSize:20,
                  padding: 5
                }}
              >
                {this.state.label}...
              </Animated.Text>
            </Animated.View>

            <Animated.View
              style={{
                width: this.state.screenWidth * 0.15,
                height: this.state.screenWidth * 0.15,

                backgroundColor: Logo_BG,
                transform: [{ rotate: spin }],
                justifyContent: "center",
                alignItems: "center",
                borderRadius: radius
              }}
            >
              <Logo
                width={this.state.screenWidth * 0.08}
                height={this.state.screenWidth * 0.08}
                color={AppColors.app_color.white}
              />
            </Animated.View>
          </Animated.View>
        </Animated.View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00000066"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Loading);
