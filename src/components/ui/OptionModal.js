import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Modal } from "react-native";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { AppText } from "@ui";

export default class OptionModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showOptionModal: this.props.visible,
      options: this.props.options,
      label: this.props.label ? this.props.label : null
    };
  }
  componentWillReceiveProps = nextProps => {
    this.setState({
      showOptionModal: nextProps.visible,
      options: nextProps.options,
      label: nextProps.label ? nextProps.label : null
    });
  };
  render() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showOptionModal}
        onRequestClose={() => {
          this.setState({ showOptionModal: false });
        }}
      >
        <TouchableOpacity
          onPress={() => {
            this.setState({ showOptionModal: false });
          }}
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: AppColors.app_color.black + "77"
          }}
        >
          <View
            style={{
              width: "70%",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            {this.state.label && (
              <View
                style={{
                  marginVertical: 5,
                  backgroundColor: AppColors.app_color.primary,
                  borderBottomStartRadius: 50,
                  borderBottomEndRadius: 50,
                  padding: 10,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <AppText
                  style={[
                    AppFonts.h5_regular,
                    { color: AppColors.app_color.white, textAlign: "center" }
                  ]}
                >
                  {this.state.label}
                </AppText>
              </View>
            )}
            {this.state.options.map((currentOption, index, array) => {
              return (
                <TouchableOpacity
                  key={'Option_' + index}
                  style={{
                    marginVertical: 5,
                    backgroundColor: AppColors.app_color.white,
                    borderRadius: 10,
                    borderWidth:0.5,
                    borderColor:AppColors.app_color.primary,
                    padding: 10,
                    width: "90%",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  onPress={() => {
                    this.setState({ showOptionModal: false });
                    this.props.hide();
                    currentOption.action();
                  }}
                >
                  <AppText
                    style={[
                      AppFonts.h5_regular,
                      { color: AppColors.app_color.primary,textAlign:'center' }
                    ]}
                  >
                    {currentOption.name}
                  </AppText>
                </TouchableOpacity>
              );
            })}

            <TouchableOpacity
              style={{
                marginVertical: 5,
                backgroundColor: AppColors.app_color.white,
                borderRadius: 10,
                borderWidth: 0.5,
                borderColor: AppColors.app_color.primary,
                padding: 10,
                width: "90%",
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => {
                this.setState({ showOptionModal: false });
                this.props.hide()
              }}
            >
              <AppText
                style={[
                  AppFonts.h5_regular,
                  { color: AppColors.app_color.primary }
                ]}
              >
                Cancel
              </AppText>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}
