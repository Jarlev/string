import { Platform } from "react-native";

function lineHeight(fontSize) {
  const multiplier = fontSize > 20 ? 0.1 : 0.33;
  return parseInt(fontSize + fontSize * multiplier, 10);
}

const base = {
  fontSize: 14,
  //lineHeight: lineHeight(14),
  ...Platform.select({
    ios: {
          fontFamily: "AppFont-Regular"
    },
    android: {
        fontFamily: "AppFont-Regular"
    }
  })
};
const base_bold = {
  fontSize: 14,
  //lineHeight: lineHeight(14),
  ...Platform.select({
    ios: {
      fontFamily: "AppFont-Bold"
    },
    android: {
        fontFamily: "AppFont-Bold"
    }
  })
};
const regular = {
  fontSize: 14,
  //lineHeight: lineHeight(14),
  ...Platform.select({
    ios: {
      fontFamily: "Regular"
    },
    android: {
      fontFamily: "Regular"
    }
  })
};

export default {
  regular: {
      ...regular,
    
  },
  h1_regular: {
      ...regular,
    fontSize: regular.fontSize * 1.75
    //lineHeight: lineHeight(base.fontSize * 2)
  },
    h2_regular: {
        ...regular,
        fontSize: regular.fontSize * 1.5
    //lineHeight: lineHeight(base.fontSize * 1.75)
  },
    h3_regular: {
        ...regular,
        fontSize: regular.fontSize * 1.25
    //lineHeight: lineHeight(base.fontSize * 1.5)
  },
    h4_regular: {
        ...regular,
    fontSize: regular.fontSize * 1.1
    //lineHeight: lineHeight(base.fontSize * 1.25)
  },
    h5_regular: { ...regular },
  base: { ...base },
  h1: {
    ...base,
    fontSize: base.fontSize * 1.75
    //lineHeight: lineHeight(base.fontSize * 2)
  },
  h2: {
    ...base,
    fontSize: base.fontSize * 1.5
    //lineHeight: lineHeight(base.fontSize * 1.75)
  },
  h3: {
    ...base,
    fontSize: base.fontSize * 1.25
    //lineHeight: lineHeight(base.fontSize * 1.5)
  },
  h4: {
    ...base,
    fontSize: base.fontSize * 1.1
    //lineHeight: lineHeight(base.fontSize * 1.25)
  },
  h5: { ...base },
  base_bold: { ...base_bold },
  h1_bold: {
    ...base_bold,
    fontSize: base_bold.fontSize * 1.75
    //lineHeight: lineHeight(base_bold.fontSize * )
  },
  h2_bold: {
    ...base_bold,
    fontSize: base_bold.fontSize * 1.5
    //lineHeight: lineHeight(base_bold.fontSize * 1.75)
  },
  h3_bold: {
    ...base_bold,
    fontSize: base_bold.fontSize * 1.25
    //lineHeight: lineHeight(base_bold.fontSize * 1.5)
  },
  h4_bold: {
    ...base_bold,
    fontSize: base_bold.fontSize * 1.1
    //lineHeight: lineHeight(base_bold.fontSize * 1.25)
  },
  h5_bold: { ...base_bold }
};
