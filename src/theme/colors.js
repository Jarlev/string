/* App colors */

const app = {
  app_color: {
    primary: "#71c2ff",
    secondary: "#ffffff",
    black: "#000000",
    white: "#ffffff",
    grey: "#A9A9A9"
  }
};

export default app;
