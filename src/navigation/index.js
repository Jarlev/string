import React from "react";
import { Actions, Scene, ActionConst } from "react-native-router-flux";
import { Text } from "react-native";
import AppLaunch from "@containers/splash/AppLaunch";
import screen1 from "@containers/screen1";
import screen2 from "@containers/screen2";
import screen3 from "@containers/screen3";
import screen4 from "@containers/screen4";
import Rough from "@containers/rough/Loading";
import Loading from "@containers/authentication/Loading";
import TiedUps from "@containers/app/TiedUps";
import LocateUser from "@containers/app/LocateUser";
import Account from "@containers/app/Account";
import UserProfile from "@containers/app/UserProfile";
import Notifications from "@containers/app/Notifications";
import RequestTieUps from "@containers/app/RequestTieUps";
import Login from "@containers/authentication/Login";
import Signup from "@containers/authentication/Signup";
import Main from "@containers/main";
import { AppColors, AppSizes } from "@theme";
import { Ionicons } from "@expo/vector-icons";

const TabIcon = ({ title, focused }) => {
  //console.log(focused + " " + title);
  var icon = "md-home";
  if (title == "Home") {
    icon = "md-home";
  } else if (title == "Notification") {
    icon = "ios-notifications";
  } else if (title == 'Account'){
    icon = "md-person";
  }
  return (
    <Ionicons
      name={icon}
      size={25}
      color={focused ? AppColors.app_color.primary : "black"}
    />
  );
  // return <Text style={{ color: focused ? AppColors.app_color.primary : "black" }}>{title}</Text>;
};
export default Actions.create(
  <Scene key={"root"}>
    {/* <Scene
      hideNavBar
      key={"Rough"}
      component={Rough}
      analyticsDesc={"AppLaunch: Launching App"}
    /> */}
    <Scene
      hideNavBar
      key={"Splash"}
      component={AppLaunch}
      analyticsDesc={"AppLaunch: Launching App"}
    />
    
    <Scene hideNavBar key={"RequestTieUps"} component={RequestTieUps} />
    <Scene hideNavBar key={"Login"} component={Login} />
    <Scene hideNavBar key={"Signup"} component={Signup} />
    <Scene hideNavBar key={"screen2"} component={screen2} />
    <Scene hideNavBar key={"screen2"} component={screen1} />
    <Scene hideNavBar key={"splash"} component={screen1} />
    <Scene hideNavBar key={"LocateUser"} component={LocateUser} />
    <Scene hideNavBar key={"UserProfile"} component={UserProfile} />

    <Scene
      key="App"
      hideNavBar={true}
      tabs={true}
      showLabel={false}
      tabBarStyle={{
        backgroundColor: AppColors.app_color.white,
        elevation: 5,
        height: 40
      }}
    >
      <Scene
        key="Home"
        title="Home"
        hideNavBar
        showLabel={false}
        icon={TabIcon}
      >
        <Scene key="TiedUps" component={TiedUps} />
      </Scene>
      <Scene
        key="Notification"
        title="Notification"
        hideNavBar
        showLabel={false}
        icon={TabIcon}
        direction="vertical"
        component={Notifications}
      >
        {/* <Scene key="Notifications" component={Notifications} /> */}
      </Scene>
      <Scene
        key="Account"
        title="Account"
        hideNavBar
        showLabel={false}
        icon={TabIcon}
        direction="vertical"
        component={Account}
      />
    </Scene>
  </Scene>
);
