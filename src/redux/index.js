import { combineReducers } from 'redux';
import router from '@redux/router/reducer';
import user from "@redux/user/reducer";
const appReducer = combineReducers({
    router,
    user
});
const rootReducer = (state, action) => {
    const newState = (action.type === 'RESET') ? undefined : state;
    return appReducer(newState, action);
};

export default rootReducer;

