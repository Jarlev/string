const initialState = {};
export default function userReducer(state = initialState, action) {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case "FONT_LOADED": {
      if (action.data) {
        const input = action.data;
        return { ...state, fontLoaded: action.data };
      }
      return {};
    }
    // ...other actions
    case "USER_UPDATED": {
      if (action.data) {
        const input = action.data;
        return { ...state, user_data: action.data };
      }
      return {};
    }
    default:
      return state;
  }
}
