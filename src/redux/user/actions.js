import { firebase } from "@constants/";

export const fontLoader=(payload)=> {
  return async dispatch => {
    try {
      return dispatch({
        type: "FONT_LOADED",
        data: true
      });
    } catch (error) {
      console.log("useractions : fontLoader : error => " + error);
    }
  };
}
export const turnUserInfoListenerOn =  (uid) => {
  return async (dispatch) => {
  var userInfo;

  await firebase
    .database()
    .ref("UsersList/")
    .child(uid)
    .on("value", snapshot => {
      if (snapshot.val()) {
        console.log("getUserInfo" + JSON.stringify(snapshot.val()));

        userInfo = snapshot.val();
        console.log("userActions : userinfo " + JSON.stringify(userInfo));

        try {
         dispatch({ type: "USER_UPDATED", data: userInfo });
        } catch (error) {
          console.log("useractions : turnUserInfoListenerOn : error => " + error);
        }
      } else {
      }
    });
  }
    
};
export const turnUserInfoListenerOff =  uid => {
  return async (dispatch) => {
         await firebase
           .database()
           .ref("UsersList/")
           .child(uid)
           .off("value");
  }
       };