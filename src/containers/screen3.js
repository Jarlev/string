import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  ImageBackground,
  Animated,
  FlatList
} from "react-native";
import { Actions } from "react-native-router-flux";
import { firebase } from "@constants/";
import { AppColors, AppSizes } from "@theme";
import { LinearGradient, Svg } from "expo";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isrefreshing: false,
      datasource: [],
      screenWidth: 0,
      screenHeight: 0
    };
  }
  componentDidMount = () => {
    // //console.log('on screen 3');
    // this.readUserData();
    this.writeUserData();
  };

  componentWillUnmount = () => {
    firebase
      .database()
      .ref("UsersList/")
      .off("value");
    //console.log("unmounted");
  };
  refreshdata = async (list_data )=> {
    // //console.log("refresh" + JSON.stringify(list_data));
    for(let i=0;i<list_data.length;i++){
        await firebase
      .database()
      .ref("UsersList/")
        .orderByKey()
      .equalTo(''+list_data[i].key)
      .once("value")
      .then(snapshot => {
        if (snapshot.val()) {
            snapshot.forEach((child)=>{
                //console.log("key" + child.val().UserName);
                list_data[i].UserName = child.val().UserName;
            })
            
        } else {
          //console.log("error");
        }
      });}
      //console.log('refreshed  '+ JSON.stringify(list_data));
      
  };
  readUserData = async () => {

    
    // var usersList = [];
    // await firebase
    //   .database()
    //   .ref("TiedUps/123")
    //   .on("value", snapshot => {
    //     let list_data = [];
    //     // //console.log(snapshot.val());
    //     snapshot.forEach(function(childSnapshot) {
    //       //   //console.log(childSnapshot.val().email + childSnapshot.key);
    //       var item = childSnapshot.val();
    //       item.key = childSnapshot.key;
    //       list_data.push(item);
    //     });
    //     // //console.log("here" + JSON.stringify(list_data));
    //     this.refreshdata(list_data);
    //   });

    // await firebase
    //   .database()
    //   .ref("UsersList/")
    //   .orderByChild("email")
    //   .startAt("kjg")
    //   .limitToLast(1)
    //   .once("value")
    //   .then(snapshot => {
    //     if (snapshot.val()) {
    //       snapshot.forEach(child => {
    //         //console.log("key" + child.key);
    //       });
    //     } else {
    //       //console.log("error");
    //     }
    //   });
  };
  writeUserData() {
    firebase
      .database()
      .ref("Notifications/")
      .child("123")
      .push({
        'type': "1",
        'isOpened' : false,
        'requestedUserId' : '1'
      });
    // firebase
    //   .database()
    //   .ref("UsersList/")
    //   .child("2")
    //   .set({
    //     UserName: "Vivek"
    //   });

    //   .then(data => {
    //     //success callback
    //     //console.log("data ", data);
    //   })
    //   .catch(error => {
    //     //error callback
    //     //console.log("error ", error);
    //   });
  }
  getRandomColor = () => {
    var ColorCode =
      "rgba(" +
      Math.floor(Math.random() * 100) +
      "," +
      Math.floor(Math.random() * 100) +
      "," +
      Math.floor(Math.random() * 100) +
      ",1)";
    return ColorCode;
  };
  renderListItem = ({ item }) => {
    var ColorCode = this.getRandomColor();
    return (
      <View
        style={{
          marginVertical: 5,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <LinearGradient
          start={[1, 0]}
          colors={[
            AppColors.app_color.white,
            AppColors.app_color.white,
            AppColors.app_color.white,
            ColorCode
          ]}
          style={{
            backgroundColor: "transparent",
            height: 100,
            width: "95%",
            justifyContent: "center",
            alignItems: "center",
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            borderColor: AppColors.app_color.primary,
            borderWidth: 1,
            padding: 10
          }}
        >
          <View
            style={{
              backgroundColor: AppColors.app_color.white + "00",
              height: "100%",
              width: "100%",
              justifyContent: "center",
              alignItems: "flex-start",
              borderTopStartRadius: 10,
              borderTopEndRadius: 10
            }}
          >
            <View
              style={{
                height: 60,
                width: 60,
                borderRadius: 30,
                elevation: 5,
                backgroundColor: AppColors.app_color.white + "cc",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text style={{ color: ColorCode, fontSize: 30 }}>
                {item.key.charAt(0).toUpperCase()}
              </Text>
            </View>
          </View>
        </LinearGradient>
        <TouchableOpacity
          style={{
            elevation: 5,
            backgroundColor: AppColors.app_color.white,
            height: 30,
            width: "95%",
            justifyContent: "center",
            alignItems: "center",
            borderBottomStartRadius: 10,
            borderBottomEndRadius: 10
          }}
        >
          <Text style={{ color: ColorCode, fontSize: 15 }}>Locate</Text>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: AppColors.app_color.white
        }}
      >
        <View style={{ width: "100%" }}>
          <FlatList
            onRefresh={() => {
              this.setState({ isrefreshing: true });
              setTimeout(() => {
                this.setState({ isrefreshing: false });
              }, 3000);
            }}
            refreshing={this.state.isrefreshing}
            renderItem={this.renderListItem}
            // renderItem={({ item }) => <Text>{item.key}</Text>}
            data={[
              { key: "vicky" },
              { key: "baskar" },
              { key: "vivek" },
              { key: "tharun" },
              { key: "shree" },
              { key: "sowmya" },
              { key: "sarah" },
              { key: "vinola" },
              { key: "peace" },
              { key: "sam" },
              { key: "ram" }
            ]}
            keyExtractor={(item, index) => item + index}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
