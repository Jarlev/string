import React from "react";
import { firebase } from "@constants/";
import { StyleSheet, Platform, Image, Text, View, Button } from "react-native";
export default class Main extends React.Component {
  state = { currentUser: null };
  componentDidMount() {
     
    const { currentUser } = firebase.auth();
    this.setState({ currentUser });
    //console.log("current user information =>" + JSON.stringify(currentUser));
  }
    handleSignOut = () => {
    // TODO: Firebase stuff...
    firebase
      .auth()
        .signOut();
  };
  render() {
    const { currentUser } = this.state;
    return (
      <View style={styles.container}>
        <Text>Hi {currentUser && currentUser.email}!</Text>
        <Button title="Sign Up" onPress={this.handleSignOut} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
