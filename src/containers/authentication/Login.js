import React from "react";
import {
  TouchableOpacity,
  Animated,
  StyleSheet,
  Text,
  TextInput,
  View,
  StatusBar,
  Button
} from "react-native";
import { Actions } from "react-native-router-flux";
import { firebase } from "@constants/";
import { AppColors, AppSizes } from "@theme";
import { AppText, Loading} from '@ui'
import Logo from "@components/svgs/Logo";
import AppName from "@components/svgs/AppName";
import { LinearGradient, Svg } from "expo";
export default class Login extends React.Component {
  state = {
    email: "",
    password: "",
    errorMessage: null,
    screenHeight: 0,
    screenWidth: 0,
    isLoading:false
  };
  handleLogin = async() => {
    this.setState({
      isLoading:true
    })
    // TODO: Firebase stuff...
    const { email, password } = this.state;
    await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(current_user => {
        if (current_user.user.emailVerified == false) {
            this.setState({

                alertMessage:
                    "Verification mail has been sent to " +
                    current_user.user.email +
                    ".\nAccount will be activated once it is verified."
            });
            setTimeout(() => {
                this.setState({ alertMessage: null });
            }, 4000);
        } else if (current_user.user.emailVerified == true) {
          Actions.App({ type: "reset" });
        }
      })
      .catch(error => {
        this.setState({ errorMessage: error.message });
        setTimeout(() => {
          this.setState({ errorMessage: null });
        }, 3000);
      });
      this.setState({
        isLoading:false
      })
  };
  onLayout = async e => {
    var width = this.state.screenWidth;
    await this.setState({
      screenWidth: e.nativeEvent.layout.width,
      screenHeight: e.nativeEvent.layout.height
    });
  };
  render() {
    var width = this.state.screenWidth;
    var height = this.state.screenHeight;
    return (
      <Animated.View onLayout={this.onLayout} style={styles.container}>
        {/* <Animated.View
          style={
            {
              width: this.state.screenWidth,
              height: this.state.screenHeight * 0.5,
              backgroundColor: AppColors.app_color.primary,
              position: "absolute",
              borderTopEndRadius: this.state.screenWidth / 2,
              bottom: 0,
              left: 0
            } // borderTopStartRadius: this.state.screenWidth / 2,
          }
        >
          <Logo
            width={this.state.screenWidth}
            height={this.state.screenWidth}
          />
        </Animated.View> */}
        <AppName width={width * 0.4} height={width * 0.3} />
        {this.state.errorMessage && (
          <View
            style={{
              width: width,
              top:StatusBar.currentHeight,
              backgroundColor: "red",
              position: "absolute",
              elevation: 15
            }}
          >
            <AppText
              style={[

                { fontSize: 12, color: AppColors.app_color.white, padding: 5, textAlign: 'center' }
              ]}
            >
              {this.state.errorMessage}
            </AppText>
          </View>
        )}
        {this.state.alertMessage && (
          <View
            style={{
              width: width,
            
              backgroundColor: "#f6be00",
              position: "absolute",
              top: StatusBar.currentHeight
            }}
          >
            <AppText
              style={[

                { fontSize: 12, color: AppColors.app_color.white, padding: 5, textAlign: 'center' }
              ]}
            >
              {this.state.alertMessage}
            </AppText>
            
          </View>
        )}
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            padding: 5,
            paddingVertical: 20,
            width: "90%"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              backgroundColor: AppColors.app_color.white,
              borderColor: AppColors.app_color.primary,
              borderWidth: 1,
              borderRadius: 20,
              width: "90%",
              padding: 5,
              paddingHorizontal:10,
              height: 35,
              marginBottom: 10
            }}
          >
            <TextInput
              style={styles.textInput}
              autoCapitalize="none"
              placeholder="Email"
              onChangeText={email => this.setState({ email })}
              value={this.state.email}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              backgroundColor: AppColors.app_color.white,
              borderColor: AppColors.app_color.primary,
              borderWidth: 1,
              borderRadius: 20,
              width: "90%",
              padding: 5,
              paddingHorizontal: 10,
              height: 35,
              marginBottom: 10
            }}
          >
            <TextInput
              style={styles.textInput}
              secureTextEntry
              autoCapitalize="none"
              placeholder="Password"
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
            />
          </View>
          <TouchableOpacity
            onPress={this.handleLogin}
            style={{ width: "90%", alignItems: "center", height: 35 }}
          >
            <LinearGradient
              colors={[
                AppColors.app_color.primary,
                AppColors.app_color.primary,
                AppColors.app_color.white
              ]}
              start={[1, 1]}
              style={{
                flex: 1,
                borderRadius: 20,
                borderWidth: 1,
                borderColor: AppColors.app_color.primary,
                padding: 5,
                width: "100%",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text style={{ color: AppColors.app_color.white, fontSize: 16 }}>
                Login
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          <Text
            style={{
              color: AppColors.app_color.primary,
              fontSize: 12,
              padding: 5,
              marginTop: 10
            }}
          >
            {" Don't have an account?"}
            <Text
              onPress={() => Actions.Signup()}
              style={{ color: AppColors.app_color.primary, fontSize: 14 }}
            >
              {" Sign Up"}
            </Text>
          </Text>
        </View>
        <Loading visible={this.state.isLoading} label= {'Logging In'}/>
      </Animated.View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: AppColors.app_color.white
  },
  textInput: {
    color: AppColors.app_color.primary,
    flex: 1,
    marginHorizontal: 5
  }
});
