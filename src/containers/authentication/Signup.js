import React from "react";
import { firebase } from "@constants/";
import {
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  TextInput,
  View,
  Button,
  Alert
} from "react-native";
import { AppColors, AppSizes } from "@theme";
import { AppText, Loading} from '@ui';
import Logo from "@components/svgs/Logo";
import AppName from "@components/svgs/AppName";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";
export default class SignUp extends React.Component {
  state = {
    email: "",
    screenWidth: 0,
    screenHeight: 0,
    password: "",
    errorMessage: null,
    isLoading : false
  };
  handleSignUp = async() => {
    this.setState({
      isLoading:true
    })
    // TODO: Firebase stuff...
    await firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((user) => {
        firebase
          .auth()
          .currentUser.sendEmailVerification()
          .then(
            ()=> {
                Alert.alert('String','Account has been successfully created.\nverification mail has been sent to ' + user.user.email + '.' )
                this.setState({ successMessage : 'Account has been successfully created.\nverification mail has been sent to '+user.user.email+'.' });
            },
            function(error) {
              // An error happened.
            }
          )
          .catch((err)=>{
            Alert.alert('String', 'Error Occured...'+err.message)
           
          });
      })
      .catch(error => this.setState({ errorMessage: error.message }));
    //console.log("handleSignUp");
    this.setState({
      isLoading: false
    })
  };
  onLayout = async e => {
    var width = this.state.screenWidth;
    await this.setState({
      screenWidth: e.nativeEvent.layout.width,
      screenHeight: e.nativeEvent.layout.height
    });
  };
  render() {
    var width = this.state.screenWidth;
    var height = this.state.screenHeight;
    return (
      <View onLayout={this.onLayout} style={styles.container}>
        <AppName width={width * 0.4} height={width * 0.3} />
        {this.state.errorMessage && (
          <View
            style={{
              width: width,
              top: StatusBar.currentHeight,
              backgroundColor: "red",
              position: "absolute",
              elevation: 15
            }}
          >
            <AppText
              style={[

                { fontSize: 12, color: AppColors.app_color.white, padding: 5, textAlign: 'center' }
              ]}
            >
              {this.state.errorMessage}
            </AppText>
          </View>
        )}
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            padding: 5,
            paddingVertical: 20,
            width: "90%"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              backgroundColor: AppColors.app_color.white,
              borderColor: AppColors.app_color.primary,
              borderWidth: 1,
              borderRadius: 20,
              width: "90%",
              padding: 5,
              paddingHorizontal: 10,
              height: 35,
              marginBottom: 10
            }}
          >
            <TextInput
              placeholder="Email"
              autoCapitalize="none"
              style={styles.textInput}
              onChangeText={email => this.setState({ email })}
              value={this.state.email}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              backgroundColor: AppColors.app_color.white,
              borderColor: AppColors.app_color.primary,
              borderWidth: 1,
              borderRadius: 20,
              width: "90%",
              padding: 5,
              paddingHorizontal: 10,
              height: 35,
              marginBottom: 10
            }}
          >
            <TextInput
              secureTextEntry
              placeholder="Password"
              autoCapitalize="none"
              style={styles.textInput}
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
            />
          </View>
          <TouchableOpacity
            onPress={this.handleSignUp}
            style={{ width: "90%", alignItems: "center", height: 35 }}
          >
            <LinearGradient
              colors={[
                AppColors.app_color.primary,
                AppColors.app_color.primary,
                AppColors.app_color.white
              ]}
              start={[1, 1]}
              style={{
                flex: 1,
                borderRadius: 20,
                borderWidth: 1,
                borderColor: AppColors.app_color.primary,
                padding: 5,
                width: "100%",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text style={{ color: AppColors.app_color.white, fontSize: 16 }}>
                Sign Up
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          <Text
            style={{
              color: AppColors.app_color.primary,
              fontSize: 12,
              padding: 5,
              marginTop: 10
            }}
          >
            {" Already have an account?"}
            <Text
              onPress={() => Actions.Login()}
              style={{ color: AppColors.app_color.primary, fontSize: 14 }}
            >
              {" Login"}
            </Text>
          </Text>
        </View>
        {this.state.successMessage && (
          <View
            style={{
              width: width,
              height:height,
              padding: 5,
              backgroundColor: "green",
              position: "absolute",
              top: StatusBar.currentHeight,
              justifyContent:'center',
              alignItems:'center'
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: AppColors.app_color.white
              }}
            >
              {this.state.successMessage}
            </Text>
          </View>
        )}
        <Loading visible={this.state.isLoading} label={'Signing Up'}/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: AppColors.app_color.white
  },
  textInput: {
    color: AppColors.app_color.primary,
    flex: 1,
    marginHorizontal: 5
  }
});
