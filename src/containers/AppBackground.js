import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import { LinearGradient } from "expo";
import { AppColors, AppSizes } from "@theme";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      screenWidth: this.props.style.width,
      screenHeight: this.props.style.width
    };
  }
  render() {
    //console.log("kshdf" + JSON.stringify(this.props.style));

    return (
      <View style={[{ backgroundColor: "transparent" }, this.props.style]}>
        {/* <Image
          style={this.props.style}
          source={{
            uri:
              "https://images.pexels.com/photos/414916/pexels-photo-414916.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
          }}
        /> */}
        <LinearGradient
          start={[1, 0]}
                colors={[AppColors.app_color.secondary, AppColors.app_color.primary]}
          style={[this.props.style, { backgroundColor: "transparent" }]}
        />
        {/* <LinearGradient
          colors={[
            AppColors.app_color.black + "66",
            AppColors.app_color.white + "66"
          ]}
          style={[this.props.style, { backgroundColor: "transparent" }]}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
