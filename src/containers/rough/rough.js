import React from "react";
import { firebase } from "@constants/";
import {
  Animated,
  Easing,
  Modal,
  StyleSheet,
  Platform,
  Image,
  ImageBackground,
  Text,
  View,
  Button,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StatusBar,
  Alert
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AppText, OptionModal } from "@ui";
import { connect } from "react-redux";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";
import { MapView, Location } from "expo";

const mapStateToProps = state => ({
  fontLoaded: state.user.fontLoaded
});

// Any actions to map to the component?
const mapDispatchToProps = {};

class Rough extends React.Component {
  state = {
    screenWidth: 0,
    screenHeight: 0
  };
  constructor(props) {
    super(props);
  }

  componentDidMount = async () => {};
  componentWillUnmount = () => {};

  render() {
    var width = this.state.screenWidth;
    var height =
      width > this.state.screenHeight ? width : this.state.screenHeight;

    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          alignItems: "center",

          marginTop: StatusBar.currentHeight,
          flex: 1,
          backgroundColor: "red"
        }}
      >
        <ScrollView>
          <View
            style={{
              width: width * 0.95,
              flex: 1,
              margin: width * 0.025,
              borderRadius: 10,
              backgroundColor: "white",
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent:'center',
              alignItems:'center'
            }}
          >
            {[1,2,3,4,5,6].map((index , value, arr)=>{
              return(
                <View
              style={[{
                width: (width * 0.95) / 2 - 7.5,
                height: (width * 0.95) / 2 - 7.5,
                backgroundColor: "blue",
                marginHorizontal:5,
                marginVertical:2.5
              },
              (index%2)==0 && {marginLeft : 0}]}
            />
            )
            })
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Rough);
