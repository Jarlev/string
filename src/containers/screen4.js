import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Modal } from "react-native";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { AppText, OptionModal} from "@ui";

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <OptionModal
          options={[
            {
              name: "test1",
              action: () => {
                alert("test1");
              }
            },
            {
              name: "test2",
              action: () => {
                alert("test2");
              }
            }
          ]}
          visible={true}
          
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

