import React from "react";
import {
  Easing,
  StyleSheet,
  Animated,
  Text,
  View,
  TouchableOpacity,
  Image
} from "react-native";
import {  OfflineNotice } from '@ui'
import { connect } from "react-redux";
import { firebase } from "@constants/";
import Logo from "@components/svgs/Logo";
import AppName from "@components/svgs/AppName";
import { AppColors, AppSizes } from "@theme";
import { Actions } from "react-native-router-flux";
import { LinearGradient, Svg, Font } from "expo";

import * as UserActions from "@redux/user/actions";
const mapStateToProps = state => ({});

// Any actions to map to the component?
const mapDispatchToProps = {
  fontLoader: UserActions.fontLoader
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.state = {
      screenWidth: 0,
      screenHeight: 0,
      show_app_name: false
    };
  }
  async componentDidMount() {
    await Font.loadAsync({
      // 'any_name' : require('path_to_your_font_file')
      "AppFont-Regular": require("@fonts/Ananda.ttf"),
      "AppFont-Bold": require("@fonts/Ananda_Black.ttf"),
      'Regular': require("@fonts/Fontin_Sans_B_45b.otf")
    });
    this.props.fontLoader();
  

   
  }
  animate() {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 3,
      duration: 3000,
      easing: Easing.ease
    }).start(() => {
      //   this.animate();
      this.setState({
        show_app_name: true
      });
      setTimeout(() => {
        firebase.auth().onAuthStateChanged(user => {
          if (user && user.emailVerified == true) {
            var userdata = JSON.parse(JSON.stringify(user));
            console.log("userdata", JSON.stringify(userdata));
            firebase
              .database()
              .ref("UsersList/")
              .child(userdata.uid)
              .update({
                lastLoginAt: userdata.lastLoginAt,
                verified: true
              });
            // Actions.UserProfile({ uid: 'PQGwVCeb6wSc52Euh8Md8Vx4HtG2',showRequest:true});
            Actions.App();
          } else if (user && user.emailVerified == false) {
            var userdata = JSON.parse(JSON.stringify(user));
            console.log("userdata", JSON.stringify(userdata));
            firebase
              .database()
              .ref("UsersList/")
              .child(userdata.uid)
              .update({
                locationSharing:true,
                email: userdata.email,
                userName: userdata.email.match(/^([^@]*)@/)[1],
                lastLoginAt: userdata.lastLoginAt,
                verified: false
              });
            Actions.Login();
          } else {
            Actions.Login({ type: 'reset'});
          }
        });
      }, 3000);
    });
  }
  onLayout = async e => {
    var width = this.state.screenWidth;
    await this.setState({
      screenWidth: e.nativeEvent.layout.width,
      screenHeight: e.nativeEvent.layout.height
    });
    if (width == 0) {
      this.animate();
    }
  };

  render() {
    const Left = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [
        this.state.screenWidth / 2 - this.state.screenWidth * 0.1,
        0,
        this.state.screenWidth - this.state.screenWidth * 0.2,
        this.state.screenWidth / 2 - this.state.screenWidth * 0.1
      ]
    });
    const Top = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],

      outputRange: [
        this.state.screenHeight / 2 - this.state.screenWidth * 0.1,
        this.state.screenHeight / 2 - this.state.screenWidth * 0.1,
        this.state.screenHeight / 2 - this.state.screenWidth * 0.1
      ]
    });
    const spin = this.animatedValue.interpolate({
      inputRange: [0, 3],
      outputRange: ["0deg", "360deg"]
    });
    const radius = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        (this.state.screenWidth * 0.3) / 2,
        20,
        this.state.screenWidth * 0.3
      ]
    });
    const Logo_BG = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        "rgba(113, 194, 255, 1)",
        "rgba(255, 255, 255, 0)",
        "rgba(113, 194, 255, 1)"
      ]
    });
    const Container_BG = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        "rgba(255, 255, 255, 1)",
        "rgba(113, 194, 255, 1)",
        "rgba(255, 255, 255, 1)"
      ]
    });
    const Logo_opacity = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [1, 1, 1, 0]
    });
    const App_name_opacity = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [0, 0, 0, 1]
    });
    return (
      <Animated.View
        onLayout={this.onLayout}
        style={[styles.container, { backgroundColor: Container_BG }]}
      >
        <Animated.View style={{ opacity: App_name_opacity }}>
          <AppName
            width={this.state.screenWidth * 0.4}
            height={this.state.screenWidth * 0.4}
            color={"rgba(113, 194, 255, 1)"}
          />
        </Animated.View>
        <Animated.View
          style={
            {
              opacity: Logo_opacity,
              position: "absolute",
              width: this.state.screenWidth * 0.2,
              height: this.state.screenWidth * 0.2,
              top: Top,
              left: Left,
              backgroundColor: Logo_BG,
              transform: [{ rotate: spin }],
              justifyContent: "center",
              alignItems: "center",
              borderRadius: radius
            } // left: this.state.screenWidth / 2 - this.state.screenWidth * 0.15,
          }
        >
          <Animated.View
            style={{
              width: this.state.screenWidth * 0.1,
              height: this.state.screenWidth * 0.1,
              transform: [{ rotate: spin }],
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Logo
              width={this.state.screenWidth * 0.1}
              height={this.state.screenWidth * 0.1}
              color={AppColors.app_color.white}
            />
          </Animated.View>
        </Animated.View>
        <OfflineNotice />
      </Animated.View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
