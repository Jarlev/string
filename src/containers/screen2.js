import React from "react";
import {
  Easing,
  StyleSheet,
  Animated,
  Text,
  View,
  TouchableOpacity,
  Image
} from "react-native";
import Logo from '@components/svgs/Logo';
import { Actions } from "react-native-router-flux";
import { LinearGradient, Svg } from "expo";
import { AppColors, AppSizes } from "@theme";
import AppBackground from "./AppBackground";
import svvg from './screen3'
export default class App extends React.Component {
  constructor() {
    super();
    this.animatedValue = new Animated.Value(0);
    this.state = {
      screenWidth: 0,
      screenHeight: 0
    };
  }
  animate() {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 3,
      duration: 3000,
      easing: Easing.ease
    }).start(() => {
      this.animate();
    });
    // this.animatedValue.setValue(0);
    // Animated.spring(this.animatedValue, {
    //   toValue: 3,
    //   damping:40,
    //  duration:5000

    // }).start(()=>this.animate());
  }

  onLayout = async e => {
    var width = this.state.screenWidth;
    await this.setState({
      screenWidth: e.nativeEvent.layout.width,
      screenHeight: e.nativeEvent.layout.height
    });
    if (width == 0) {
      this.animate();
    }
  };

  render() {
    const Left = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [
        this.state.screenWidth / 2 - this.state.screenWidth * 0.15,
        0,
        this.state.screenWidth - this.state.screenWidth * 0.3,
        this.state.screenWidth / 2 - this.state.screenWidth * 0.15
      ]
    });
    const Top = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],

      outputRange: [
        this.state.screenHeight / 2 - this.state.screenWidth * 0.15,
        this.state.screenHeight / 2 - this.state.screenWidth * 0.15,
        this.state.screenHeight / 2 - this.state.screenWidth * 0.15
      ]
    });
    const spin = this.animatedValue.interpolate({
      inputRange: [0, 3],
      outputRange: ["0deg", "360deg"]
    });
    const radius = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        (this.state.screenWidth * 0.3) / 2,
        20,
        this.state.screenWidth * 0.3
      ]
    });
    const Logo_BG = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [
        "rgba(113, 194, 255, 0.5)",
        "rgba(255, 255, 255, 1)",
        "rgba(255, 255, 255, 1)",
        "rgba(113, 194, 255, 0.5)"
      ]
    });
    const Logo_Color = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [
        "rgba(255, 255, 255, 1)",
        "rgba(113, 194, 255, 0.5)",
        "rgba(113, 194, 255, 0.5)",
        "rgba(255, 255, 255, 1)",
      
      ]
    });
    const Container_BG = this.animatedValue.interpolate({
      inputRange: [0, 1.5, 3],
      outputRange: [
        "rgba(255, 255, 255, 1)",
        "rgba(113, 194, 255, 0.5)",
        "rgba(255, 255, 255, 1)"
      ]
    });
    //console.log(Left + "  " + Top);

    return <Animated.View onLayout={this.onLayout} style={[styles.container, { backgroundColor: Container_BG }]}>
        <Animated.View style={{ position: "absolute", width: this.state.screenWidth * 0.3, height: this.state.screenWidth * 0.3, top: Top, left: this.state.screenWidth / 2 - this.state.screenWidth * 0.15, backgroundColor: Logo_BG, transform: [{ rotate: spin }], justifyContent: "center", alignItems: "center", borderRadius: radius }}>
          <Animated.View
            style={{
              width: this.state.screenWidth * 0.2,
              height: this.state.screenWidth * 0.2,
              transform: [{ rotate: spin }],
              justifyContent: "center",
              alignItems: "center"
            }}
          >
          <Logo width={100} height={100} color={'red'} />
          </Animated.View>
        </Animated.View>
      </Animated.View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
