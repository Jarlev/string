import React from "react";
import { firebase } from "@constants/";
import {
  Alert,
  Easing,
  StyleSheet,
  Platform,
  Image,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Animated,
  Modal
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AppText, OptionModal } from "@ui";
import { connect } from "react-redux";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";
const mapStateToProps = state => ({
  fontLoaded: state.user.fontLoaded
});

// Any actions to map to the component?
const mapDispatchToProps = {};
class RequestTieUps extends React.Component {
  state = {
    currentUser: null,
    isrefreshing: true,
    datasource: [],
    searchString: "",
    noData: 0,
    screenWidth: 0,
    screenHeight: 0,
    showAlertModal: false,
    RequestedUser: {}
  };
  componentDidMount = () => {
    
    const { currentUser } = firebase.auth();
    this.setState({ currentUser });
    setTimeout(() => {
    this._input.focus();
    }, 200);
    
  };
  handleSignOut = () => {
    firebase.auth().signOut();
  };

  getUsers = async searchString => {
    var UsersList = [];
    await firebase
      .database()
      .ref("UsersList/")
      .orderByChild("email")
      .startAt(searchString)
      .endAt(searchString + "\uf8ff")
      .once("value")
      .then(snapshot => {
        if (snapshot.val()) {
          snapshot.forEach(child => {
            //console.log("key" + child.val().userName);
            var childVal = child.val();
            var user = {};
            user.uid = child.key;
            user.userName = childVal.userName;
            user.email = childVal.email;
            user.verified = childVal.verified
            UsersList.push(user);
          });
          this.setState({
            datasource: UsersList,
            isrefreshing: false,
            noData: 0
          });
        } else {
          this.setState({
            noData: 1
          });
        }
      });
    console.log("searched users =>" + JSON.stringify(UsersList));
  };
  
  renderListItem = ({ item }) => {
    if (item.uid != this.state.currentUser.uid && item.verified == true) {
      return (
        <FadeView
          style={{
            marginVertical: 5,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            onPress={() => {
              {
               Actions.UserProfile({uid:item.uid, showRequest:true})
              }
            }}
            style={{
              backgroundColor: AppColors.app_color.white + "00",
              flexDirection: "row",
              height: 40,
              width: "95%",
              justifyContent: "flex-start",
              alignItems: "center",
              borderBottomStartRadius: 20,
              borderTopStartRadius: 20,
              elevation: 2,
              padding: 5
            }}
          >
            <View
              style={{
                height: 32,
                width: 32,
                borderRadius: 16,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: AppColors.app_color.primary
              }}
            >
              <AppText
                style={[
                  AppFonts.h3_regular,
                  { color: AppColors.app_color.white }
                ]}
              >
                {item.userName.charAt(0).toUpperCase()}
              </AppText>
            </View>
            <View>
              <Image
                style={{
                  width: 32,
                  height: 32,
                  borderRadius: 16,
                  borderColor: AppColors.app_color.primary,
                  borderWidth: 1
                }}
                source={{ uri: "https://newsladder.net/wp-content/uploads/2018/07/HD-Movies.jpg" }}
              />
            </View>
            <View
              style={{
                height: "100%",
                flex: 1,
                justifyContent: "center",
                alignItems: "flex-start",
                padding: 10
              }}
            >
              <AppText
                style={[
                  AppFonts.h5_regular,
                  { color: AppColors.app_color.primary, textAlign: "left" }
                ]}
              >
                {item.userName}
              </AppText>
              <AppText
                numberOfLines={1}
                ellipsizeMode={"middle"}
                style={[
                  AppFonts.h5_regular,
                  { color: AppColors.app_color.primary, textAlign: "left" }
                ]}
              >
                {item.email}
              </AppText>
            </View>
          </TouchableOpacity>
        </FadeView>
      );
    }
  };
  render() {
    const { currentUser } = this.state;
    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          marginTop: StatusBar.currentHeight,
          flex: 1,
          backgroundColor: AppColors.app_color.white
        }}
      >
        <FadeView style={{ alignItems: "center", marginVertical: 5 }}>
          <View
            style={{
              height: 40,
              backgroundColor: AppColors.app_color.white + "00",
              flexDirection: "row",
              width: "95%",
              justifyContent: "flex-start",
              alignItems: "center",
              borderRadius: 31,
              elevation: 2,
              padding: 5
            }}
          >
            <Ionicons
              onPress={() => {
                Actions.pop();
              }}
              style={{ padding: 5 ,marginHorizontal:5}}
              name="md-arrow-back"
              size={20}
              color={AppColors.app_color.primary}
            />
            <TextInput
              autoFocus={true}
              ref={(c) => this._input = c}
              style={{
                color: AppColors.app_color.primary,
                flex: 1,
                marginHorizontal: 5
              }}
              autoCapitalize="none"
              placeholder="Search With Email"
              onChangeText={searchString => {
                if (searchString.trim().length != 0) {
                  this.getUsers(searchString);
                } else {
                  this.setState({ datasource: [], noData: 0 });
                }
                this.setState({ searchString });
              }}
              value={this.state.searchString}
            />
          </View>
        </FadeView>
        <View
          style={{
            flex: 1,
            justifyContent: "flex-start",
            alignItems: "center"
          }}
        >
          {this.state.noData == 0 ? (
            <View style={{ width: "100%" }}>
              <FlatList
                onRefresh={async () => {
                  await firebase
                    .database()
                    .ref("TiedUps/123")
                    .once("value", snapshot => {
                      this.setState({ isrefreshing: true });

                      let list_data = [];

                      snapshot.forEach(function(childSnapshot) {
                        var item = childSnapshot.val();
                        item.uid = childSnapshot.key;
                        list_data.push(item);
                      });

                      this.refreshdata(list_data);
                    });
                }}
                refreshing={this.state.isrefreshing}
                renderItem={this.renderListItem}
                // renderItem={({ item }) => <Text>{item.key}</Text>}
                data={this.state.datasource}
                keyExtractor={(item, index) => item + index}
              />
            </View>
          ) : (
            <AppText
              style={[
                AppFonts.h3_regular,
                { padding: 5, color: AppColors.app_color.primary }
              ]}
            >
              No Match Found
            </AppText>
          )}
        </View>
        <OptionModal
          label={
            "Do you wanna request " +
            this.state.RequestedUser.userName +
            " for their Location?"
          }
          visible={this.state.showAlertModal}
          options={[
            {
              name: "Ok",
              action: () => this.handleRequest()
            }
          ]}
          hide={() => {
            this.setState({ showAlertModal: false });
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestTieUps);

class FadeView extends React.Component {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
  }
  componentDidMount = () => {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 3,
      duration: 500,
      easing: Easing.ease
    }).start();
  };
  render = () => {
    const opacity = this.animatedValue.interpolate({
      inputRange: [0, 1, 2, 3],
      outputRange: [0, 0.4, 0.7, 1]
    });
    return (
      <Animated.View style={[this.props.style, { opacity: opacity }]}>
        {this.props.children}
      </Animated.View>
    );
  };
}
