import React from "react";
import { firebase } from "@constants/";
import {
  Animated,
  Easing,
  Modal,
  StyleSheet,
  Platform,
  Image,
  ImageBackground,
  Text,
  View,
  Button,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Alert
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AppText, OptionModal, Loading , FadeView} from "@ui";
import { connect } from "react-redux";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";
import { MapView, Location } from "expo";

const mapStateToProps = state => ({
  fontLoaded: state.user.fontLoaded
});

// Any actions to map to the component?
const mapDispatchToProps = {};

class UserProfile extends React.Component {
  state = {
    screenWidth: 0,
    screenHeight: 0,
    userInfo: {},
    currentUser: {},
    showAlertModal: false,
    showRequest: false,
    showLocate: false,
    isLoading:true
  };
  constructor(props) {
    super(props);
  }

  componentDidMount = async () => {
    const { currentUser } = await firebase.auth();
    await this.setState({ currentUser, isLoading:false });
    this.turnUserInfoListenerOn(this.props.uid);
    this.getAction();
  };
  getAction = async () => {
    await firebase
      .database()
      .ref("TiedUps/" + this.state.currentUser.uid)
      .orderByKey()
      .equalTo(this.props.uid)
      .once("value")
      .then(snapshot => {
        if (snapshot.val()) {
          var snapshotValue = snapshot.val();
          console.log(snapshotValue);

          snapshot.forEach(child => {
            var childValue = child.val();
            if (childValue.Accepted && childValue.Accepted == true) {
              this.setState({ showLocate: true });
            }
          });
        } else {
          this.setState({
            showRequest: true
          });
        }
      });
  };
  componentWillUnmount = () => {
    this.turnUserInfoListenerOFF(this.props.uid);
  };
  turnUserInfoListenerOn = async uid => {
    await firebase
      .database()
      .ref("UsersList/")
      .child(uid)
      .on("value", snapshot => {
        if (snapshot.val()) {
          console.log("getUserInfo" + JSON.stringify(snapshot.val()));
          this.setState({
            userInfo: snapshot.val()
          });
        } else {
        }
      });
  };
  turnUserInfoListenerOFF = async uid => {
    await firebase
      .database()
      .ref("UsersList/")
      .child(uid)
      .off("value");
  };
  handleRequest = async () => {
    var uid = this.props.uid;
    await firebase
      .database()
      .ref("TiedUps/")
      .child(this.state.currentUser.uid)
      .child(uid)
      .update({
        Accepted: false
      });

    await firebase
      .database()
      .ref("Notifications/")
      .child(uid)
      .push({
        type: 1,
        isOpened: false,
        userId: this.state.currentUser.uid
      });
    Alert.alert("String", "Location request Sent");
    Actions.pop();
  };

  handleSignOut = () => {
    firebase.auth().signOut();
  };

  render() {
    var width = this.state.screenWidth;
    var height =
      width > this.state.screenHeight ? width : this.state.screenHeight;
    var userName =
      this.state.userInfo && this.state.userInfo.userName
        ? this.state.userInfo.userName
            .toUpperCase()
            .split("")
            .join(" ")
        : "";
    var email =
      this.state.userInfo && this.state.userInfo.email
        ? this.state.userInfo.email
        : "";
    var showRequest = this.state.showRequest;
    var showLocate = this.state.showLocate;
    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          alignItems: "center",

          marginTop: StatusBar.currentHeight,
          flex: 1,
          backgroundColor: AppColors.app_color.white
        }}
      >
        {!this.state.showRequest &&  !this.state.showLocate && (
          <View
            style={{
              width: width,

              backgroundColor: "red",

              elevation: 15
            }}
          >
            <AppText
              style={[
                {
                  fontSize: 12,
                  color: AppColors.app_color.white,
                  padding: 5,
                  textAlign: "center"
                }
              ]}
            >
              {userName + " has not accepted your request"}
            </AppText>
          </View>
        )}
        <ImageBackground
          source={{
            uri:
              "https://newsladder.net/wp-content/uploads/2018/07/HD-Movies.jpg"
          }}
          style={{
            resizeMode: "cover",
            height: height * 0.7,
            width: width * 0.95,
            marginTop: 10,
            backgroundColor: "white",
            elevation: 10,
            borderRadius: 10,
            justifyContent: "flex-end",
            alignItems: "center"
          }}
          imageStyle={{ borderRadius: 10 }}
        >
          <View
            style={{
              height: 62,
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              top: 0
            }}
          >
            <Ionicons
              onPress={() => {
                Actions.pop();
              }}
              style={{ padding: 10, position: "absolute", left: 5 }}
              name="md-arrow-back"
              size={25}
              color={AppColors.app_color.primary}
            />
          </View>
          <LinearGradient
            start={[0, 0]}
            end={[0, 1]}
            colors={["transparent", AppColors.app_color.black + "aa"]}
            style={{
              backgroundColor: "transparent",
              flexDirection: "row",
              height: "20%",
              width: "100%",
              position: "absolute",
              borderRadius: 10,
              borderBottomEndRadius: 10,
              bottom: 0,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <AppText
              style={[
                AppFonts.h2_regular,
                {
                  color: AppColors.app_color.white,
                  padding: 5,
                  textAlign: "center"
                }
              ]}
            >
              {userName}
            </AppText>
          </LinearGradient>
        </ImageBackground>
        <View
          style={{
            flexDirection: "row",
            height: height * 0.1,
            width: width * 0.8,
            backgroundColor: AppColors.app_color.white,
            elevation: 2,
            borderBottomStartRadius: 10,
            borderBottomEndRadius: 10,
            justifyContent: "center",
            alignItems: "center",
            borderColor: AppColors.app_color.primary,
            borderWidth: 1
          }}
        >
          <Ionicons
            onPress={() => {
              Actions.pop();
            }}
            style={{ padding: 5 }}
            name="md-mail"
            size={20}
            color={AppColors.app_color.primary}
          />
          <AppText
            style={[
              AppFonts.h4_regular,
              { color: AppColors.app_color.primary }
            ]}
          >
            {email}
          </AppText>
        </View>
        {showRequest && (
          <TouchableOpacity
            onPress={() => {
              this.setState({ showAlertModal: true });
            }}
            style={{
              height: height * 0.08,
              width: width * 0.5,
              backgroundColor: AppColors.app_color.primary,
              elevation: 0,
              borderBottomStartRadius: 10,
              borderBottomEndRadius: 10,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <AppText
              style={[
                AppFonts.h4_regular,
                { color: AppColors.app_color.white }
              ]}
            >
              REQUEST
            </AppText>
          </TouchableOpacity>
        )}
        {showLocate && (
          <TouchableOpacity
            onPress={() => {
              Actions.LocateUser({ foreignUserUid: this.props.uid });
            }}
            style={{
              height: height * 0.08,
              width: width * 0.5,
              backgroundColor: AppColors.app_color.primary,
              elevation: 0,
              borderBottomStartRadius: 10,
              borderBottomEndRadius: 10,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <AppText
              style={[
                AppFonts.h4_regular,
                { color: AppColors.app_color.white }
              ]}
            >
              LOCATE
            </AppText>
          </TouchableOpacity>
        )}
        <OptionModal
          label={
            "Do you wanna request " +
            this.state.userInfo.userName +
            " for their Location?"
          }
          visible={this.state.showAlertModal}
          options={[
            {
              name: "Ok",
              action: () => this.handleRequest()
            }
          ]}
          hide={() => {
            this.setState({ showAlertModal: false });
          }}
        />
        <Loading visible={this.state.isLoading} label={'Getting Profile'} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);
