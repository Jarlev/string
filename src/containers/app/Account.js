import React from "react";
import { firebase } from "@constants/";
import {
  ScrollView,
  Animated,
  Easing,
  Modal,
  StyleSheet,
  Platform,
  Image,
  ImageBackground,
  Text,
  View,
  Button,
  TouchableOpacity,
  FlatList,
  StatusBar,
  TextInput,
  Alert,
  Switch,
  KeyboardAvoidingView
} from "react-native";
import { Ionicons, Entypo, MaterialCommunityIcons } from "@expo/vector-icons";
import { AppText, OptionModal, FadeView } from "@ui";
import { connect } from "react-redux";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";
import { MapView, Location } from "expo";
import * as UserActions from "@redux/user/actions";
const mapStateToProps = state => ({
  fontLoaded: state.user.fontLoaded,
  user_data: state.user.user_data
});

// Any actions to map to the component?
const mapDispatchToProps = {
  turnUserInfoListenerOn: UserActions.turnUserInfoListenerOn
};

class Account extends React.Component {
  state = {
    screenWidth: 0,
    screenHeight: 0,
    userInfo: {},
    currentUser: {},
    showAlertModal: false,
    editUserName: false,
    errorMessage: null
  };
  constructor(props) {
    super(props);
  }

  componentDidMount = async () => {
    const { currentUser } = await firebase.auth();
    await this.setState({ currentUser });
    this.setState({
      userInfo: this.props.user_data
    });
  };
  componentWillReceiveProps = nextProps => {
    this.setState({
      userInfo: nextProps.user_data
    });
  };
  componentWillUnmount = () => {
    // this.turnUserInfoListenerOFF(this.state.currentUser.uid);
  };

  handleSignOut = () => {
    firebase.auth().signOut();
  };
  editUserName = async () => {
    var emptyString = /^\s*$/;
    var alphabetsAndSpace = /^[A-Za-z\s]+$/;
    if (emptyString.test(this.state.userNameTemp)) {
      this.setState({ errorMessage: "User Name should not be empty." });
      setTimeout(() => {
        this.setState({ errorMessage: null });
      }, 3000);
    } else if (!alphabetsAndSpace.test(this.state.userNameTemp.trim())) {
      this.setState({
        errorMessage: "User Name should contain only Alphabets and Spaces."
      });
      setTimeout(() => {
        this.setState({ errorMessage: null });
      }, 3000);
    } else {
      this.toggleEditUserName();
      await firebase
        .database()
        .ref("UsersList/")
        .child(this.state.currentUser.uid)
        .update({ userName: this.state.userNameTemp.trim() });
    }
  };
  shareLocationToggle = async () => {
    await firebase
      .database()
      .ref("UsersList/")
      .child(this.state.currentUser.uid)
      .update({ locationSharing: !this.state.userInfo.locationSharing });
  };
  
  toggleEditUserName = () => {
    var editUsername = !this.state.editUserName;
    console.log(editUsername);

    this.setState({
      editUserName: editUsername
    });
  };
  render() {
    var width = this.state.screenWidth;
    var height =
      width > this.state.screenHeight
        ? (width / this.state.screenHeight) * width
        : this.state.screenHeight;
    var userName =
      this.state.userInfo && this.state.userInfo.userName
        ? this.state.userInfo.userName
            .toUpperCase()
            .split("")
            .join(" ")
        : "";
    var email =
      this.state.userInfo && this.state.userInfo.email
        ? this.state.userInfo.email
        : "";
    var locationSharing =
      this.state.userInfo && this.state.userInfo.locationSharing
        ? this.state.userInfo.locationSharing
        : false;
    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          alignItems: "center",
          marginTop: StatusBar.currentHeight,
          flex: 1,
          backgroundColor: AppColors.app_color.white
        }}
      >
        <ScrollView>
          <View
            style={{
              alignItems: "center"
            }}
          >
            {this.state.errorMessage && (
              <View
                style={{
                  width: width,

                  backgroundColor: "red",
                  position: "absolute",
                  elevation: 15
                }}
              >
                <AppText
                  style={[
                    {
                      fontSize: 12,
                      color: AppColors.app_color.white,
                      padding: 5,
                      textAlign: "center"
                    }
                  ]}
                >
                  {this.state.errorMessage}
                </AppText>
              </View>
            )}
            <ImageBackground
              source={{
                uri:
                  "https://newsladder.net/wp-content/uploads/2018/07/HD-Movies.jpg"
              }}
              style={{
                resizeMode: "cover",
                height: height * 0.7,
                width: width,
                backgroundColor: "white",
                elevation: 10,
                justifyContent: "flex-end",
                alignItems: "center"
              }}
              imageStyle={{}}
            >
              <LinearGradient
                start={[0, 0]}
                end={[0, 1]}
                colors={["transparent", AppColors.app_color.black + "aa"]}
                style={{
                  backgroundColor: "transparent",
                  flexDirection: "row",
                  height: "100%",
                  width: "100%",
                  position: "absolute",
                  borderBottomEndRadius: 10,
                  bottom: 0,
                  justifyContent: "flex-start",
                  alignItems: "flex-end"
                }}
              >
                {!this.state.editUserName && (
                  <FadeView>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        flexWrap: "wrap"
                      }}
                    >
                      <AppText
                        style={[
                          AppFonts.h2_regular,
                          {
                            color: AppColors.app_color.white,
                            padding: 20,
                            textAlign: "left"
                          }
                        ]}
                      >
                        {userName}
                      </AppText>
                      <MaterialCommunityIcons
                        onPress={() => {
                          this.setState({
                            userNameTemp: this.state.userInfo.userName
                          });
                          this.toggleEditUserName();
                        }}
                        style={{ padding: 5 }}
                        name="pencil-circle"
                        size={30}
                        color={AppColors.app_color.primary}
                      />
                    </View>
                  </FadeView>
                )}
                {this.state.editUserName && (
                  <KeyboardAvoidingView
                    style={styles.container}
                    behavior="padding"
                    enabled
                  >
                    <FadeView>
                      <View
                        style={{
                          height: 40,
                          backgroundColor: AppColors.app_color.white,
                          flexDirection: "row",
                          width: width - 40,
                          justifyContent: "flex-start",
                          alignItems: "center",
                          borderRadius: 31,
                          elevation: 2,
                          padding: 5,
                          margin: 20
                        }}
                      >
                        <TextInput
                          style={{
                            flex: 1,
                            color: AppColors.app_color.primary,
                            paddingHorizontal: 20
                          }}
                          autoCapitalize="none"
                          placeholder="User Name"
                          onChangeText={searchString => {
                            this.setState({ userNameTemp: searchString });
                          }}
                          value={this.state.userNameTemp}
                        />
                        <Ionicons
                          onPress={() => {
                            this.editUserName();
                          }}
                          style={{ padding: 5 }}
                          name="ios-checkmark-circle"
                          size={25}
                          color={AppColors.app_color.primary}
                        />
                        <Entypo
                          onPress={() => {
                            this.toggleEditUserName();
                          }}
                          style={{ padding: 5 }}
                          name="cross"
                          size={25}
                          color={AppColors.app_color.primary}
                        />
                      </View>
                    </FadeView>
                  </KeyboardAvoidingView>
                )}
              </LinearGradient>
            </ImageBackground>
            <View
              style={{
                flexDirection: "row",
                width: width * 0.95,
                margin: 5,
                marginTop: 10,
                backgroundColor: AppColors.app_color.white,
                elevation: 2,
                borderTopStartRadius: 30,
                borderTopEndRadius: 30,
                justifyContent: "center",
                alignItems: "center",
                borderColor: AppColors.app_color.primary,
                borderWidth: 1
              }}
            >
              <Ionicons
                onPress={() => {}}
                style={{ padding: 5 }}
                name="md-mail"
                size={15}
                color={AppColors.app_color.primary}
              />
              <AppText
                style={[
                  AppFonts.h5_regular,
                  { color: AppColors.app_color.primary, padding: 5 }
                ]}
              >
                {email}
              </AppText>
            </View>
            <View
              style={{
                flexDirection: "row",
                width: width * 0.95,
                margin: 5,
                marginTop: 0,
                backgroundColor: AppColors.app_color.white,
                elevation: 2,
                justifyContent: "center",
                alignItems: "center",
                borderColor: AppColors.app_color.primary,
                borderWidth: 1
              }}
            >
              <AppText
                style={[
                  AppFonts.h5_regular,
                  { color: AppColors.app_color.primary, padding: 5 }
                ]}
              >
                LOCATION SHARING
              </AppText>
              <Switch
                value={locationSharing}
                onValueChange={this.shareLocationToggle}
                thumbColor={AppColors.app_color.primary} // tintColor={AppColors.app_color.primary+'aa'}
                trackColor={AppColors.app_color.primary + "aa"}
              />
            </View>
            <TouchableOpacity
              onPress={this.handleSignOut}
              style={{
                flexDirection: "row",
                width: width * 0.95,
                margin: 5,
                marginTop: 0,
                backgroundColor: AppColors.app_color.primary,
                elevation: 2,
                borderBottomStartRadius: 30,
                borderBottomEndRadius: 30,
                justifyContent: "center",
                alignItems: "center",
                borderColor: AppColors.app_color.white,
                borderWidth: 1
              }}
            >
              <AppText
                style={[
                  AppFonts.h5_regular,
                  { color: AppColors.app_color.white, padding: 5 }
                ]}
              >
                LOG OUT
              </AppText>
            </TouchableOpacity>
            <OptionModal
              label={""}
              visible={this.state.showAlertModal}
              options={[{ name: "Ok", action: () => this.handleRequest() }]}
              hide={() => {
                this.setState({ showAlertModal: false });
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Account);
