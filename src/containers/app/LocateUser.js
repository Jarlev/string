import React from "react";
import { firebase } from "@constants/";
import {
  Animated,
  Easing,
  Modal,
  StyleSheet,
  Platform,
  Image,
  Text,
  View,
  Button,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Alert
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AppText } from "@ui";
import { connect } from "react-redux";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";
import { MapView, Location, Permissions } from "expo";

const mapStateToProps = state => ({
  fontLoaded: state.user.fontLoaded
});

// Any actions to map to the component?
const mapDispatchToProps = {};

const GEOLOCATION_OPTIONS = {
  enableHighAccuracy: true,
  timeout: 1000,
  maximumAge: 1000
};
class LocateUsers extends React.Component {
  state = {
    screenWidth: 0,
    screenHeight: 0,
    location: { coords: { latitude: 0, longitude: 0 } },
    foreignUser: {}
  };
  constructor(props) {
    super(props);
    this.locationListener;
  }

  componentDidMount = async () => {
    const { currentUser } = await firebase.auth();
    await this.setState({ currentUser });
    this.getForignUserInfo(this.props.foreignUserUid);
    this.syncLocation(true);
  };
  componentWillUnmount = () => {
    this.syncLocation(false);
  };
  syncLocation = async sync => {
    if (sync) {
      let { status } = await Permissions.askAsync(Permissions.LOCATION);
      if (status !== "granted") {
        console.log("location permission denied");
      } else {
        this.locationListener = await Location.watchPositionAsync(
          GEOLOCATION_OPTIONS,
          this.locationChanged
        );
      }
    } else {
      this.locationListener != null && this.locationListener.remove();
    }
  };
  locationChanged = location => {
    console.log(
      "location changed =>" + JSON.stringify(location ? location : {})
    );

    (region = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      latitudeDelta: 0.1,
      longitudeDelta: 0.05
    }),
      this.setState({ location, region });
  };

  getForignUserInfo = async uid => {
    console.log(uid);

    var userdata = {};
    await firebase
      .database()
      .ref("UsersList/")
      .child(uid)
      .on("value", snapshot => {
        if (snapshot.val()) {
          console.log("getUserInfo" + JSON.stringify(snapshot.val()));
          this.setState({
            foreignUser: snapshot.val()
          });
        } else {
        }
      });
  };

  handleSignOut = () => {
    firebase.auth().signOut();
  };

  render() {
    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          marginTop: StatusBar.currentHeight,
          flex: 1,
          backgroundColor: AppColors.app_color.white
        }}
      >
        <View
          style={{
            height: 45,
            width: "100%",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Ionicons
            onPress={() => {
              Actions.pop();
            }}
            style={{ padding: 10, position: "absolute", left: 5 }}
            name="md-arrow-back"
            size={20}
            color={AppColors.app_color.primary}
          />
          <AppText
            style={[AppFonts.h2_bold, { color: AppColors.app_color.primary }]}
          >
            Locate
          </AppText>
        </View>
        <Expo.MapView
          style={{ flex: 1 }}
          showsUserLocation={true}
          region={this.state.region}
        >
          <Expo.MapView.Marker
            coordinate={{
              latitude:
                this.state.foreignUser && this.state.foreignUser.latitude
                  ? this.state.foreignUser.latitude
                  : 0,
              longitude:
                this.state.foreignUser && this.state.foreignUser.longitude
                  ? this.state.foreignUser.longitude
                  : 0
            }}
            title={
              this.state.foreignUser && this.state.foreignUser.userName
                ? this.state.foreignUser.userName
                : ""
            }
            description={""}
            anchor={{ x: 0.5, y: 0.5 }}
          >
            <View
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: 40,
                  height: 40,
                  padding: 10,
                  borderRadius: 20,

                  borderWidth: 2,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Image
                  style={{ width: 30, height: 30, borderRadius: 15 }}
                  source={{ uri: "http://www.webtezy.com/images/user.png" }}
                />
              </View>
              <AppText
                style={[
                  AppFonts.regular,
                  {
                    fontSize: 10,
                    backgroundColor: AppColors.app_color.white,
                    padding: 5,
                    borderRadius: 10,
                    borderWidth: 0.5
                  }
                ]}
              >
                {this.state.foreignUser && this.state.foreignUser.userName
                  ? this.state.foreignUser.userName
                  : ""}
              </AppText>
            </View>
          </Expo.MapView.Marker>
        </Expo.MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocateUsers);
