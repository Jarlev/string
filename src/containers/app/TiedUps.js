import React from "react";
import { firebase } from "@constants/";
import {
  Animated,
  Easing,
  Modal,
  StyleSheet,
  Platform,
  Image,
  Text,
  View,
  Button,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Alert
} from "react-native";
import { Octicons } from "@expo/vector-icons";
import { AppText, OptionModal, FadeView, Loading, OfflineNotice } from "@ui";
import { connect } from "react-redux";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";
import * as UserActions from "@redux/user/actions";
import { Constants, Location, Permissions, TaskManager } from "expo";
const mapStateToProps = state => ({
  fontLoaded: state.user.fontLoaded,
  user_data: state.user.user_data
});
const GEOLOCATION_OPTIONS = {
  enableHighAccuracy: true,
  timeout: 1000,
  maximumAge: 100
};
// Any actions to map to the component?
const mapDispatchToProps = {
  turnUserInfoListenerOn: UserActions.turnUserInfoListenerOn,
  turnUserInfoListenerOff: UserActions.turnUserInfoListenerOff

};
class TiedUps extends React.Component {
  state = {
    currentUser: null,
    isrefreshing: true,
    isLoading:true,
    datasource: [],
    showOptionModal: false,
    options: [],
    screenWidth: 0,
    screenHeight: 0,
    noData: 0
  };
  constructor(props) {
    super(props);
  }

  componentWillUnmount=()=>{
    this.turnTiedUpListenerOOff();
    this.props.turnUserInfoListenerOff(this.state.currentUser.uid);
  }
  syncLocation=async(sync)=>{
    if(sync){
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      console.log("location permission denied");
    } else {
      // TaskManager.defineTask(STRING_LOCAION_UPDATE, ({ data: { locations }, error }) => {
      //   if (!error) {
      //     // check `error.message` for more details.
      //     return;
      //   }
      //   // firebase
      //   //   .database()
      //   //   .ref("UsersList/")
      //   //   .child(this.state.currentUser.uid)
      //   //   .update({
      //   //     'lattitude': "1",
      //   //     'isOpened': false,
      //   //     'requestedUserId': '1'
      //   //   });
      //   console.log('String Received new locations', locations);
      // });
      // navigator.geolocation.watchPosition(
      //   async position => {
      //     const { latitude, longitude } = position.coords;

      //     console.log("position");

      //     console.log(position);

      //     await firebase
      //       .database()
      //       .ref("UsersList/")
      //       .child(this.state.currentUser.uid)
      //       .update({
      //         lattitude: latitude,
      //         longitude: longitude
      //       });
      //   },
      //   error => console.log("location : error "+JSON.stringify(error)),
      //   { enableHighAccuracy: true, timeout: 1000, maximumAge: 1000 }
      // );
      // Location.startLocationUpdatesAsync();
      this.locationListener = await Location.watchPositionAsync(GEOLOCATION_OPTIONS, this.locationChanged);
      // Location.startLocationUpdatesAsync(STRING_LOCAION_UPDATE, { accuracy: 6, timeInterval: 1000, distanceInterval :1});
    }
  }else{
      this.locationListener != null && this.locationListener.remove()
  }
  }
  componentDidMount = async () => {
    const { currentUser } = await firebase.auth();
    await this.setState({ currentUser });
    //console.log("current user information =>" + JSON.stringify(currentUser));
    this.turnTiedUpListenerOn();
    this.props.turnUserInfoListenerOn(this.state.currentUser.uid);

    await this.setState({
      user_data: this.props.user_data
    })
    this.syncLocation(this.state.user_data && this.state.user_data.locationSharing)
  };

  componentWillReceiveProps = async(nextProps)=>{
    this.setState({ user_data: nextProps.user_data });
    this.syncLocation(nextProps.user_data && nextProps.user_data.locationSharing);
   }
  componentWillUnmount=()=>{
    this.syncLocation(false)
  }
  locationChanged = location => {
    console.log(
      "location changed =>" + JSON.stringify(location ? location : {})
    );
    firebase
      .database()
      .ref("UsersList/")
      .child(this.state.currentUser.uid)
      .update({
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        timestamp: location.timestamp
      });
  };
  measureWelcome() {
    this.refs.label.measure(this.logWelcomeLayout);
  }

  logWelcomeLayout(ox, oy, width, height, px, py) {
    console.log("ox: " + ox);
    console.log("oy: " + oy);
    console.log("width: " + width);
    console.log("height: " + height);
    console.log("px: " + px);
    console.log("py: " + py);
  }
  handleSignOut = () => {
    firebase.auth().signOut();
  };
  refreshdata = async list_data => {
    // //console.log("refresh" + JSON.stringify(list_data));
    var tiedUpsdata=[];
    for (let i = 0; i < list_data.length; i++) {
      await firebase
        .database()
        .ref("UsersList/")
        .orderByKey()
        .equalTo("" + list_data[i].uid)
        .once("value")
        .then(snapshot => {
          if (snapshot.val()) {
            snapshot.forEach(child => {
              //console.log("key" + child.val().userName);
              list_data[i].userName = child.val().userName;
              list_data[i].userId = child.key;
              tiedUpsdata.push(list_data[i]);
            });
            this.setState({
              datasource: tiedUpsdata,
              isrefreshing: false,
              
              noData: 0
            });
          } else {
            //console.log("error");
          }
        });
    }
    console.log("refreshed  " + JSON.stringify(list_data));
  };
  turnTiedUpListenerOn = async () => {
    var usersList = [];
    await firebase
      .database()
      .ref("TiedUps/" + this.state.currentUser.uid)
      .on("value", snapshot => {
        this.setState({ isrefreshing: true });

        let list_data = [];
        // //console.log(snapshot.val());
        if (snapshot.val()) {
          snapshot.forEach(function(childSnapshot) {
            //   //console.log(childSnapshot.val().email + childSnapshot.key);
            var item = childSnapshot.val();
            item.uid = childSnapshot.key;
            list_data.push(item);
          });
          // //console.log("here" + JSON.stringify(list_data));
          this.refreshdata(list_data);
        } else {
          this.setState({ noData: 1});
        }
        this.setState({ isLoading: false });
      });
  };
  turnTiedUpListenerOOff = async () => {

    await firebase
      .database()
      .ref("TiedUps/" + this.state.currentUser.uid)
      .off("value")
  };
  getRandomColor = () => {
    var ColorCode =
      "rgba(" +
      Math.floor(Math.random() * 100) +
      "," +
      Math.floor(Math.random() * 100) +
      "," +
      Math.floor(Math.random() * 100) +
      ",1)";
    return ColorCode;
  };
  handleTiedUpsAction = tiedUpsdata => {
    console.log(tiedUpsdata.Accepted);

    if (!(tiedUpsdata.Accepted && tiedUpsdata.Accepted == true)) {
      Alert.alert(
        "String",
        tiedUpsdata.userName + " has not yet accepted your Location Request."
      );
    } else {
      Actions.LocateUser({ foreignUserUid: tiedUpsdata.uid });
    }
  };
  handleTiedUpsLongPressAction = tiedUpsdata => {
    if (!(tiedUpsdata.Accepted && tiedUpsdata.Accepted == true)) {
      Alert.alert(
        "String",
        tiedUpsdata.userName + " has not yet accepted your Location Request."
      );
    } else {
      var options = [
        {
          name: "Stop sharing Location with " + tiedUpsdata.userName,
          action: () => {
            firebase
              .database()
              .ref("TiedUps/")
              .child(this.state.currentUser.uid)
              .child(tiedUpsdata.userId)
              .remove();
            firebase
              .database()
              .ref("TiedUps/")
              .child(tiedUpsdata.userId)
              .child(this.state.currentUser.uid)
              .remove();
            firebase
              .database()
              .ref("Notifications/")
              .child(tiedUpsdata.userId)
              .push({
                type: 6,
                isOpened: false,
                userId: this.state.currentUser.uid
              });
          }
        },
        {
          name: "View " + tiedUpsdata.userName + "'s profile",
          action: () => {
            Actions.UserProfile({ uid: tiedUpsdata.uid, showLocate: tiedUpsdata.Accepted })
          }
        }
      ];
      this.setState({
        showOptionModal: true,
        options: options
      });
    }
  };
  renderListItem = ({ item }) => {
    var ColorCode = this.getRandomColor();

    var acceptanceColor =
      item.Accepted && item.Accepted == true ? "green" : "red";
    return (
      <FadeView>
      <TouchableOpacity
        onPress={() => {
          this.handleTiedUpsAction(item);
        }}
        onLongPress={() => {
          this.handleTiedUpsLongPressAction(item);
        }}
        style={{
          marginVertical: 5,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <View
          style={{
            backgroundColor: AppColors.app_color.white + "00",
            flexDirection: "row",
            height: 40,
            width: "95%",
            justifyContent: "flex-start",
            alignItems: "center",
            borderBottomStartRadius: 20,
            borderTopStartRadius: 20,
            elevation: 2,
            padding: 5,
            borderRightColor: acceptanceColor,
            borderRightWidth: 5
          }}
        >
          <View
            style={{
              height: 32,
              width: 32,
              borderRadius: 16,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: AppColors.app_color.primary
            }}
          >
            <AppText
              style={[
                AppFonts.h3_regular,
                { color: AppColors.app_color.white }
              ]}
            >
              {item.userName && item.userName.charAt(0).toUpperCase()}
            </AppText>
          </View>
          <View>
            <Image
              style={{
                width: 32,
                height: 32,
                borderRadius: 16,
                borderColor: AppColors.app_color.primary,
                borderWidth: 1
              }}
                source={{ uri: "https://newsladder.net/wp-content/uploads/2018/07/HD-Movies.jpg"  }}
            />
          </View>
          <View
            style={{
              height: "100%",
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <AppText
              style={[
                AppFonts.h4_regular,
                { color: AppColors.app_color.primary }
              ]}
            >
              {item.userName}
            </AppText>
          </View>
        </View>
      </TouchableOpacity>
      </FadeView>
    );
  };
  render() {
    this.props.user_data ? console.log("yser" + this.props.user_data.userName) : console.log("logged");
    
    const { currentUser } = this.state;
    var nodata = this.state.noData;
    console.log(nodata);

    return (
      <View
        ref="label"
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          marginTop: StatusBar.currentHeight,
          flex: 1,
          backgroundColor: AppColors.app_color.white
        }}
      >
        <View
          style={{
            height: 45,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <AppText
            style={[AppFonts.h2_bold, { color: AppColors.app_color.primary }]}
          >
            TiedUps
          </AppText>

          <Octicons
            onPress={() => {
              this.measureWelcome();
              this.setState({
                showOptionModal: true,

                options: [
                  {
                    name: "Request Friends",
                    action: () => Actions.RequestTieUps()
                  },
                  { name: "Log Out", action: () => this.handleSignOut() }
                ]
              });
            }}
            style={{ padding: 10, position: "absolute", right: 5 }}
            name="ellipsis"
            size={25}
            color={AppColors.app_color.primary}
          />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "flex-start",
            alignItems: "center"
          }}
        >
          <View style={{ width: "100%" }}>
            {this.state.noData == 0 ? (
              <FlatList
                onRefresh={async () => {
                  await firebase
                    .database()
                    .ref("TiedUps/" + this.state.currentUser.uid)
                    .once("value", snapshot => {
                      this.setState({ isrefreshing: true });

                      let list_data = [];

                      snapshot.forEach(function(childSnapshot) {
                        var item = childSnapshot.val();
                        item.uid = childSnapshot.key;
                        list_data.push(item);
                      });

                      this.refreshdata(list_data);
                    });
                }}
                refreshing={this.state.isrefreshing}
                renderItem={this.renderListItem}
                data={
                  this.state.datasource // renderItem={({ item }) => <Text>{item.key}</Text>}
                }
                keyExtractor={(item, index) => item + index}
              />
            ) : (
              <View
                style={{
                  height: "100%",
                  weight: "100%",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    Actions.RequestTieUps();
                  }}
                  style={{
                    height: "90%",
                    width: "90%",
                    backgroundColor: AppColors.app_color.primary,
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 20
                  }}
                >
                  <AppText
                    style={[
                      AppFonts.h3_regular,
                      {
                        padding: 5,
                        color: AppColors.app_color.white,
                        textAlign: "center"
                      }
                    ]}
                  >
                    Click here to Search and Request Location from your friends
                  </AppText>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
        <OptionModal
          options={this.state.options}
          visible={this.state.showOptionModal}
          hide={()=>{
            this.setState({
              showOptionModal:false
            })
          }}
        />

        {/* Option Modal */}
        {/* <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.showOptionModal}
          onRequestClose={() => {
            this.setState({ showOptionModal: false });
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this.setState({ showOptionModal: false });
            }}
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: AppColors.app_color.black + "77"
            }}
          >
            <View
              style={{
                width: "70%",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                style={{
                  marginVertical: 5,
                  backgroundColor: AppColors.app_color.white,
                  borderRadius: 10,
                  padding: 10,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center"
                }}
                onPress={() => {
                  this.setState({ showOptionModal: false });
                  Actions.RequestTieUps();
                }}
              >
                <AppText
                  style={[
                    AppFonts.h4_regular,
                    { color: AppColors.app_color.primary }
                  ]}
                >
                  Request Friends
                </AppText>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginVertical: 5,
                  backgroundColor: AppColors.app_color.white,
                  borderRadius: 10,
                  padding: 10,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center"
                }}
                onPress={() => {
                  this.setState({ showOptionModal: false });
                  this.handleSignOut();
                }}
              >
                <AppText
                  style={[
                    AppFonts.h4_regular,
                    { color: AppColors.app_color.primary }
                  ]}
                >
                  Log Out
                </AppText>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  marginVertical: 5,
                  backgroundColor: AppColors.app_color.white,
                  borderRadius: 10,
                  padding: 10,
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center"
                }}
                onPress={() => {
                  this.setState({ showOptionModal: false });
                }}
              >
                <AppText
                  style={[
                    AppFonts.h4_regular,
                    { color: AppColors.app_color.primary }
                  ]}
                >
                  Cancel
                </AppText>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </Modal> */}
        <Loading visible={this.state.isLoading} label={'Getting TiedUps'} />
        <OfflineNotice/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TiedUps);
