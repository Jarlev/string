import React from "react";
import { firebase } from "@constants/";
import {
  Animated,
  Easing,
  Modal,
  StyleSheet,
  Platform,
  Image,
  Text,
  View,
  Button,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Alert
} from "react-native";
import {
  MaterialCommunityIcons,
  FontAwesome,
  Entypo
} from "@expo/vector-icons";
import { AppText, FadeView } from "@ui";
import { connect } from "react-redux";
import { AppColors, AppSizes, AppFonts } from "@theme";
import { LinearGradient, Svg } from "expo";
import { Actions } from "react-native-router-flux";

const mapStateToProps = state => ({
  fontLoaded: state.user.fontLoaded
});

// Any actions to map to the component?
const mapDispatchToProps = {};
class Notifications extends React.Component {
  state = {
    currentUser: null,
    isrefreshing: true,
    datasource: [],
    showAlertModal: false,
    screenWidth: 0,
    screenHeight: 0,
    noData: 0,
    currentNotificationData: ""
  };
  componentDidMount = async () => {
    const { currentUser } = await firebase.auth();
    await this.setState({ currentUser });
    //console.log("current user information =>" + JSON.stringify(currentUser));
    this.turnNotificationListenerOn();
    // console.log("data from redux =>" + this.props.fontLoaded);
  };
  componentWillUnmount = () => {
    this.turnNotificationListenerOff();
  };
  handleSignOut = () => {
    firebase.auth().signOut();
  };
  getUserInfo = async uid => {
    // console.log(uid);

    var userdata = {};
    await firebase
      .database()
      .ref("UsersList/")
      .orderByKey()
      .equalTo(uid)
      .once("value")
      .then(async snapshot => {
        if (snapshot.val()) {
          await snapshot.forEach(async child => {
            //console.log("key" + child.val().userName);
            userdata = child.val();
          });
        } else {
        }
      });
    return userdata;
  };
  refreshdata = async list_data => {
    list_data.reverse();
    var notificationData = [];
    // //console.log("refresh" + JSON.stringify(list_data));
    for (let i = 0; i < list_data.length; i++) {
      var userData = await this.getUserInfo(list_data[i].userId);
      list_data[i] = { ...list_data[i], ...userData };
      notificationData.push(list_data[i]);
      this.setState({
        datasource: notificationData,
        isrefreshing: false,
        noData: 0
      });
    }
  };
  onRefreshNotification = async () => {
    await firebase
      .database()
      .ref("Notifications/" + this.state.currentUser.uid)
      .once("value", snapshot => {
        this.setState({ isrefreshing: true, datasource: [] });

        let list_data = [];
        // //console.log(snapshot.val());
        if (snapshot.val()) {
          snapshot.forEach(function(childSnapshot) {
            //   //console.log(childSnapshot.val().email + childSnapshot.key);
            var item = childSnapshot.val();
            item.key = childSnapshot.key;
            list_data.push(item);
          });
          this.refreshdata(list_data);
        } else {
          this.setState({
            noData: 1
          });
        }
        // //console.log("here" + JSON.stringify(list_data));
      });
  };
  turnNotificationListenerOn = async () => {
    var usersList = [];
    await firebase
      .database()
      .ref("Notifications/" + this.state.currentUser.uid)
      .on("value", snapshot => {
        this.setState({ isrefreshing: true, datasource: [] });

        let list_data = [];
        // //console.log(snapshot.val());
        if (snapshot.val()) {
          snapshot.forEach(function(childSnapshot) {
            //   //console.log(childSnapshot.val().email + childSnapshot.key);
            var item = childSnapshot.val();
            item.key = childSnapshot.key;
            list_data.push(item);
          });
          this.refreshdata(list_data);
        } else {
          this.setState({
            noData: 1
          });
        }
        // //console.log("here" + JSON.stringify(list_data));
      });
  };
  turnNotificationListenerOff = async () => {
    var usersList = [];
    await firebase
      .database()
      .ref("Notifications/" + this.state.currentUser.uid)
      .off("value");
  };
  handleNotificationAction = notificationData => {
    if (notificationData.type == 1) {
      Alert.alert(
        "String",
        "Do you wanna accept Location Request from " +
          notificationData.userName +
          " (" +
          notificationData.email +
          ") ?",
        [
          {
            text: "Accept",
            onPress: () => {
              firebase
                .database()
                .ref("TiedUps/")
                .child(notificationData.userId)
                .child(this.state.currentUser.uid)
                .update({ Accepted: true });
              firebase
                .database()
                .ref("TiedUps/")
                .child(this.state.currentUser.uid)
                .child(notificationData.userId)
                .update({ Accepted: true });
              firebase
                .database()
                .ref("Notifications/")
                .child(notificationData.userId)
                .push({
                  type: 2,
                  isOpened: false,
                  userId: this.state.currentUser.uid
                });
              firebase
                .database()
                .ref("Notifications/")
                .child(this.state.currentUser.uid)
                .child(notificationData.key)
                .update({ type: 4, isOpened: true });
            }
          },
          {
            text: "Deny",
            onPress: () => {
              firebase
                .database()
                .ref("TiedUps/")
                .child(notificationData.userId)
                .child(this.state.currentUser.uid)
                .remove();

              firebase
                .database()
                .ref("Notifications/")
                .child(notificationData.userId)
                .push({
                  type: 3,
                  isOpened: false,
                  userId: this.state.currentUser.uid
                });
              firebase
                .database()
                .ref("Notifications/")
                .child(this.state.currentUser.uid)
                .child(notificationData.key)
                .update({ type: 5, isOpened: true });
            },
            style: "cancel"
          }
        ],
        { cancelable: true }
      );
    }else{
      firebase
        .database()
        .ref("Notifications/")
        .child(this.state.currentUser.uid)
        .child(notificationData.key)
        .update({isOpened: true });
      Actions.UserProfile({uid:notificationData.userId})
    }
  };
  renderListItem = ({ item }) => {
    console.log(item);

    var gradientColors = ["transparent"];

    if (!item.isOpened) {
      gradientColors = ["#f7a400", "#fee818"];
    } else if (item.type == 2 || item.type == 4 ) {
      gradientColors = ["#d7276b", "#741ca8"];
    } else if (item.type == 3 || item.type == 5) {
      gradientColors = ["#0db4ab", "#033fab"];
    } else {
      gradientColors = ["#88b4ab", "#033fab"];
    }
    var icon = (
      <Entypo name={"bell"} size={20} color={AppColors.app_color.white} />
    );
    if (item.type == 1) {
      icon = (
        <MaterialCommunityIcons
          name={"human-handsup"}
          size={20}
          color={AppColors.app_color.white}
        />
      );
    } else if (item.type == 2 || item.type == 4) {
      icon = (
        <FontAwesome
          name={"handshake-o"}
          size={20}
          color={AppColors.app_color.white}
        />
      );
    }
    var message = "";
    if (item.type == 1) {
      message = item.userName + " is requesting your location";
    } else if (item.type == 2) {
      message = item.userName + " has accepted your Request";
    } else if (item.type == 3) {
      message = item.userName + " has rejected your Request";
    } else if (item.type == 4) {
      message = "You have accepted " + item.userName + "'s Location Request";
    } else if (item.type == 5) {
      message = "You have rejected " + item.userName + "'s Location Request";
    } else if (item.type == 6) {
      message =  item.userName + " has stopped sharing their Location";
    }
    return (
      <BlinkView
        blink={!item.isOpened}
        style={{
          marginVertical: 5,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <TouchableOpacity onPress={() => this.handleNotificationAction(item)}>
          <LinearGradient
            start={[0, 1]}
            end={[1, 0]}
            colors={gradientColors}
            style={{
              backgroundColor: AppColors.app_color.white + "00",
              flexDirection: "row",
              height: 40,
              width: "100%",
              justifyContent: "flex-start",
              alignItems: "center",
              elevation: 2
            }}
          >
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                width: "15%"
              }}
            >
              {icon}
            </View>

            <AppText
              style={[
                AppFonts.h4_regular,
                { color: AppColors.app_color.white, width: "75%", padding: 5 }
              ]}
            >
              {message}
            </AppText>
            <FontAwesome
              style={{ padding: 10, width: "10%" }}
              name={"chevron-right"}
              size={20}
              color={AppColors.app_color.white}
            />
          </LinearGradient>
        </TouchableOpacity>
      </BlinkView>
    );
  };
  render() {
    const { currentUser } = this.state;
    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{
          marginTop: StatusBar.currentHeight,
          flex: 1,
          backgroundColor: AppColors.app_color.white
        }}
      >
        <View
          style={{
            height: 45,
            width: "100%",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <AppText
            style={[AppFonts.h2_bold, { color: AppColors.app_color.primary }]}
          >
            Notifications
          </AppText>
          {/* <Octicons
            onPress={() => {
              this.setState({ showOptionModal: true });
            }}
            style={{ padding: 10, position: "absolute", right: 5 }}
            name="ellipsis"
            size={35}
            color={AppColors.app_color.primary}
          /> */}
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "flex-start",
            alignItems: "center"
          }}
        >
          <View style={{ width: "100%" }}>
            {this.state.noData == 0 ? (
              <FlatList
                onRefresh={this.onRefreshNotification}
                refreshing={this.state.isrefreshing}
                renderItem={this.renderListItem}
                data={
                  this.state.datasource // renderItem={({ item }) => <Text>{item.key}</Text>}
                }
                keyExtractor={(item, index) => item + index}
                extraData={this.state}
              />
            ) : (
              <AppText
                style={[
                  AppFonts.h3_regular,
                  {
                    padding: 5,
                    color: AppColors.app_color.primary,
                    textAlign: "center"
                  }
                ]}
              >
                No Notificaions Found
              </AppText>
            )}
          </View>
        </View>

        {/* Option Modal */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notifications);

class BlinkView extends React.Component {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
  }
  componentDidMount = () => {
    this.animatedValue.setValue(0);
    if (this.props.blink && this.props.blink == true) {
      Animated.timing(this.animatedValue, {
        toValue: 3,
        duration: 3000,
        easing: Easing.linear
      }).start();
    }
  };
  render = () => {
    var blink = this.props.blink ? this.props.blink : false;
    var opacity;
    if (blink) {
      opacity = this.animatedValue.interpolate({
        inputRange: [0, 1, 2, 3],
        outputRange: [0, 1, 0, 1]
      });
    }
    return (
      <Animated.View
        style={[this.props.style, { opacity: blink ? opacity : 1 }]}
      >
        {this.props.children}
      </Animated.View>
    );
  };
}
