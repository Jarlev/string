import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  ImageBackground,
  Animated
} from "react-native";
import { Actions } from "react-native-router-flux";
import { firebase } from "@constants/";
import { AppColors, AppSizes } from "@theme";
import { LinearGradient, Svg } from "expo";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      datasource: [],
      screenWidth: 0,
      screenHeight: 0
    };
  }
  componentDidMount = () => {
    this.readUserData();
  };

  componentWillUnmount = () => {
    firebase
      .database()
      .ref("UsersList/")
      .off("value");
    ////console.log("unmounted");
  };
  readUserData = async () => {
   
    await firebase
      .database()
      .ref("UsersList/")
      .on("value", snapshot => {
        let list_data = [];
        ////console.log(snapshot.val());
        snapshot.forEach(function(childSnapshot) {
          ////console.log(childSnapshot.val().email + childSnapshot.key);
          var item = childSnapshot.val();
          item.key = childSnapshot.key;
          list_data.push(item);
        });
        ////console.log("here" + JSON.stringify(list_data));

        // this.setState({
        //   datasource: list_data
        // });
      });
    await firebase
      .database()
      .ref("UsersList/")
      .orderByChild("email")
      .startAt('kjg')
      .limitToLast(1)
      .once("value")
      .then((snapshot)=> {
        if(snapshot.val()){
        snapshot.forEach(child => {
          ////console.log('key'+child.key);
        });}
        else{
          ////console.log('error');
          
        }
      });

  };
  writeUserData(email, fname, lname) {
    firebase
      .database()
      .ref("UsersList/")
      .push({
        email,
        fname,
        lname
      })
      .then(data => {
        //success callback
        ////console.log("data ", data);
      })
      .catch(error => {
        //error callback
        ////console.log("error ", error);
      });
  }
  render() {
    return (
      <View
        onLayout={e => {
          this.setState({
            screenWidth: e.nativeEvent.layout.width,
            screenHeight: e.nativeEvent.layout.height
          });
        }}
        style={{ flex: 1 }}
      >
     
        {/*<Svg
          width={this.state.screenWidth}
          height={this.state.screenHeight}
          viewBox="0 0 100 100"
        >
          <Svg.Path
            d="M100,100 L0,100 L0,0 C0,52.45695 45.54305,100 100,100 Z"
            children={
              <View
                style={{ width: 400, height: 400, backgroundColor: "red" }}
              />
            }
            fill={"transparent"}
          />
        </Svg>

         <ImageBackground
          style={{
            width: this.state.screenWidth,
            height: this.state.screenHeight,
            resizeMode: "contain"
          }}
          source={{
            uri:
              "https://images.pexels.com/photos/1098365/pexels-photo-1098365.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
          }}
        >
          <LinearGradient
            colors={[AppColors.app_color.primary,"transparent", ]}
            strat={[1,1]}
            style={{
              position:'absolute',
              bottom:0,
              width: this.state.screenWidth,
              height: this.state.screenHeight*0.12,
              alignItems: "center",
              
            }}
          >
            <TouchableOpacity
          onPress={() => {
            ////console.log("sdfsd");
            Actions.screen2({ type: "reset" });
          }}
        >*/}
          {this.state.datasource.map((item, i) => {
            return <Text key={i}>{item}</Text>;
          })}
        {/*</TouchableOpacity>
          </LinearGradient>
        </ImageBackground> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
